'use strict'
const express = require('express')
const router = express.Router()
const authRouter = require('./auth.route')
const mongo = require('mongoose')

router.get('/status', (req, res) => { res.send({apiStatus: 'OK', mongo:mongo.connection.readyState}) }) // api status

router.use('/auth', authRouter) // mount auth paths

module.exports = router
